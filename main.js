import axios from 'axios';

class Api {
    static async getWeather(city) {
        const response = new Weather((await axios.get(`https://api.hgbrasil.com/weather?key=1755962d&city_name=${city}`)).data.results);
        return response
    }
}

class Weather {
    constructor({ city_name, temp, date, time, description, sunrise, sunset, humidity }) {
        this.city_name = city_name,
            this.temp = temp,
            this.date = date,
            this.time = time,
            this.description = description,
            this.sunrise = sunrise,
            this.sunset = sunset,
            this.humidity = humidity
    }
}

Api.getWeather("Pelotas").then(v => { console.log(v) });